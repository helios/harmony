(defproject harmony "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/clojurescript "1.10.439"]
                 [reagent "0.8.1"]
                 [re-frame "0.10.6"]]

  :plugins [[lein-cljsbuild "1.1.7"]]

  :source-paths ["src/clj" "src/cljs" "src/cljc"]
  :resource-paths ["resources"]
  :figwheel {:css-dirs ["resources/public/css"]
             :server-port 3450}

  :profiles {:dev {:dependencies [[binaryage/devtools "0.9.10"]]
                   :plugins [[lein-figwheel "0.5.16"]]}
             :prod {}}

  :cljsbuild {:builds
              {:dev {:source-paths ["dev/cljs" "src/cljs" "src/cljc"]
                     :figwheel {:on-jsload harmony.dev/on-jsload}
                     :compiler {:output-to "resources/public/js/app.js"
                                :output-dir "resources/public/js/out"
                                :source-map true
                                :asset-path "js/out"
                                :main harmony.core
                                :optimizations :none
                                :preloads [devtools.preload]}}
               :prod {:source-paths ["src/cljs" "src/cljc"]
                      :jar true
                      :compiler {:output-to "resources/public/js/app.js"
                                 :main harmony.core
                                 :optimizations :advanced
                                 :pretty-print false}}}}

  :clean-targets ^{:protect false} ["resources/public/js/out" "target"])
