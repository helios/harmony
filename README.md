# harmony

To run in development mode

    lein figwheel
    
    
To compile the javascript

    lein cljsbuild once prod
    

## License

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
