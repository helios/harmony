(ns harmony.dev
  (:require [harmony.core :as core]))

(enable-console-print!)

(defn on-jsload []
  (core/main))
