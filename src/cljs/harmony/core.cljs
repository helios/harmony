(ns harmony.core
  (:require [reagent.core :as r]
            [re-frame.core :as re-frame]
            [harmony.events :as events]
            [harmony.index :as index]))

(enable-console-print!)


(defn ^:export main []
  (re-frame/dispatch-sync [::events/init!])
  (re-frame/clear-subscription-cache!)
  (r/render
    [index/index-component]
    (js/document.getElementById "app")))



