(ns harmony.events
  (:require [re-frame.core :as re-frame]
            [harmony.board :as board]))

(re-frame/reg-event-db
  ::rotate-tile!
  (fn [db [_cmd index]]
    (update-in db [:cells index :status] #(rem (inc %) 4))))

(re-frame/reg-event-db
  ::reset-game!
  (fn [db _]
    (board/scramble db)))

(re-frame/reg-event-db
  ::re-initialize!
  (fn [state _]
    (board/random-board (:size state))))

(re-frame/reg-event-db
  ::init!
  (fn [_state _cmd]
    (board/random-board 4)))

(re-frame/reg-event-db
  ::update-size!
  (fn [_state [_cmd value]]
    (board/random-board value)))