(ns harmony.board)

(defn tile [n-set]
  (case (count n-set)
    4 :4-tile
    3 :3-tile
    2 (if (#{#{:N :S} #{:E :W}} n-set)
        :2-tile-I
        :2-tile-L)
    1 :1-tile
    0 :0-tile))

(defn scramble [board]
  (update-in board
             [:cells]
             (fn [cells]
               (mapv #(assoc % :status (rand-int 4))
                     cells))))


(def correct-positions
  {#{:N :S :W :E} #{0 1 2 3}
   #{:W :N :E} #{0}
   #{:N :E :S} #{1}
   #{:E :S :W} #{2}
   #{:S :W :N} #{3}
   #{:N :S} #{0 2}
   #{:W :E} #{1 3}
   #{:N :E} #{0}
   #{:E :S} #{1}
   #{:S :W} #{2}
   #{:W :N} #{3}
   #{:N} #{0}
   #{:E} #{1}
   #{:S} #{2}
   #{:W} #{3}
   #{} #{0 1 2 3}})


(defn solve [board]
  (update-in board [:cells]
             (fn [cells]
               (mapv #(assoc % :status (->> % :neighbors correct-positions first))
                     cells))))

(defn correct-position? [cell]
  (contains?
    (get correct-positions (:neighbors cell))
    (:status cell)))

;;;;; switching to a different definition of correctness

(defn points-to [{:keys [tile status]}]
  (case tile
    :0-tile #{}
    :1-tile (case status
              0 #{:N}
              1 #{:E}
              2 #{:S}
              3 #{:W})
    :2-tile-I (case status
                0 #{:N :S}
                1 #{:E :W}
                2 #{:N :S}
                3 #{:E :W})
    :2-tile-L (case status
                0 #{:N :E}
                1 #{:E :S}
                2 #{:S :W}
                3 #{:N :W})
    :3-tile (case status
              0 #{:N :E :W}
              1 #{:N :S :E}
              2 #{:S :E :W}
              3 #{:N :S :W})
    :4-tile #{:N :S :W :E}))

(defn pointed-back? [{:keys [index] :as cell} {:keys [size] :as board}]
  (let [connected-directions (points-to cell)]
    (every? (fn [dir]
              (let [i (quot index size)
                    j (rem index size)]
                (case dir
                  :N (and (not= i 0)
                          (contains? (points-to (nth (:cells board)
                                                     (- index size)))
                                     :S))
                  :S (and (not= i (dec size))
                          (contains? (points-to (nth (:cells board)
                                                     (+ index size)))
                                     :N))
                  :W (and (not= j 0)
                          (contains? (points-to (nth (:cells board)
                                                     (- index 1)))
                                     :E))
                  :E (and (not= j (dec size))
                          (contains? (points-to (nth (:cells board)
                                                     (+ index 1)))
                                     :W)))))
            connected-directions)))

(defn correct? [board]
  (every? #(pointed-back? % board)
          (:cells board)))


;;;;;;;;;;;;;;;;;

(def ideal-board-composition
  {0 0
   1 0.33
   2 0.33
   3 0.33
   4 0})

(defn abs [n] (max n (- n)))

(defn board-fitness [board]
  (when (:size board)
    (let [cells-by-neighbor-count (->> (:cells board)
                                       (group-by #(count (:neighbors %))))]
      (- 1 (/ (reduce + (map (fn [[n-count ideal-count]]
                               (abs (- ideal-count (/ (count (get cells-by-neighbor-count n-count))
                                                      (count (:cells board))))))
                             ideal-board-composition))
              4)))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; rather than defining a sparsity pattern, we construct 2 link matrices, one for the rows and one for the columns
;; Row link matrix: N-1 x N    : a_{i,j} means: Tiles t_{i,j} and t_{i,j+1} are connected
;; Column link matrix: N x N-1 : a_{i,j} means: Tiles t_{i,j} and t_{i+1,j} are connected


(defn connected-neighbors [{:keys [size row-links column-links]} index]
  (let [i (quot index size)
        j (rem index size)
        north? (and (not= i 0)
                    (= 1 (get column-links (+ j (* (dec i) size)))))
        south? (and (not= i (dec size))
                    (= 1 (get column-links index)))
        west? (and (not= j 0)
                   (= 1 (get row-links (+ (dec j) (* i (dec size))))))
        east? (and (not= j (dec size))
                   (= 1 (get row-links (+ j (* i (dec size))))))]
    (cond-> #{}
      north? (conj :N)
      south? (conj :S)
      west? (conj :W)
      east? (conj :E))))

(defn create-board [{:keys [size] :as bl}]
  {:size size
   :cells (->> (range (Math/pow size 2))
               (map (partial connected-neighbors bl))
               (map-indexed (fn [idx n-set]
                              {:value (if (empty? n-set) 0 1)
                               :i (quot idx size)
                               :j (rem idx size)
                               :index idx
                               :neighbors n-set
                               :tile (tile n-set)
                               :status (rand-int 4)}))
               (vec))})


(defn random-board [size]
  (create-board
    {:size size
     :row-links (into [] (repeatedly (* size (dec size)) #(rand-int 2)))
     :column-links (into [] (repeatedly (* size (dec size)) #(rand-int 2)))}))

(comment
  (def board-links
    ;; Used for documentation: this is how the structure of the board looks-like
    {:size 2
     :row-links [1 0]
     :column-links [0 1]})

  (def board-created
    ;; this is for documentation purposes: this should be the result of (create-board board-links), apart from status which is random
    {:size 2
     :cells [{:value 1 :neighbors #{:E} :tile :1-tile :status 0}
             {:value 1 :neighbors #{:W :S} :tile :2-tile-L :status 0}
             {:value 0 :neighbors #{} :tile :0-tile :status 0}
             {:value 1 :neighbors #{:N} :tile :1-tile :status 0}]}))